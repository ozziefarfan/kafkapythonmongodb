# Author: Ozzie Farfan
# This code is used to setup MongoDB with the proper collections for the examples
# Este codigo es usado para pre-definir las collecciones requeridas en MongoDB para 
# correr los ejemplos 

from pymongo import MongoClient
from pprint import pprint

client = MongoClient('mongodb://localhost:27017/')
db = client.mydb
collection = db.cities

pprint(collection)
print ("\nCollections Available | Colecciones disponibles")
pprint(db.list_collection_names())

print ("\nAdding cities | Adicionando ciudades")
a_city = {'name': 'Malaga', 'country': 'Spain'}
postid = collection.insert_one(a_city).inserted_id
a_city = {'name': 'Vancouver', 'country': 'Canada'}
collection.insert_one(a_city)
a_city = {'name': 'Toronto', 'country': 'Canada'}
collection.insert_one(a_city)
a_city = {'name': 'Quebec', 'country': 'Canada', 'languages':['English','French']}
collection.insert_one(a_city)
a_city = {'name': 'Madrid', 'country': 'Spain', 'languages':['Spanish','Catalan']}
collection.insert_one(a_city)


print("\nCollection list | listado de coleccion: cities")
mycities = collection.find()
for a in mycities:
   pprint(a)


